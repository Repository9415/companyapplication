# Company Manager

#### software requirements
```
Java 8
Maven 4.0
JRE
IDE IntelliJ IDEA
```

#### technology stack
```
Java 8
Maven 4.0
Vaadin 14
PostgreSQL
```

#### developer
```
name: Ivan
email: ivan@gmail.com
```

#### build application
```
mvn clean install
```

#### database initialization
```
CREATE DATABASE company_app_postgres WITH OWNER = postgres ENCODING = 'UTF8' LC_COLLATE = 'Russian_Russia.1251' LC_CTYPE = 'Russian_Russia.1251' TABLESPACE = pg_default CONNECTION LIMIT = -1;
CREATE SCHEMA IF NOT EXISTS public AUTHORIZATION postgres;
COMMENT ON SCHEMA public IS 'standard public schema';
CREATE TABLE IF NOT EXISTS public.company (id varchar(50) NOT NULL, name varchar(30) NOT NULL, inn int8 NOT NULL, address varchar(50) NULL, phonenumber int8 NULL, CONSTRAINT company_pkey PRIMARY KEY (id));
CREATE TABLE IF NOT EXISTS public.employee (id varchar(50) NOT NULL, fio varchar(70) NOT NULL, datebirth date NOT NULL, email varchar(50) NULL, companyid varchar(50) NOT NULL, CONSTRAINT employee_pkey PRIMARY KEY (id), CONSTRAINT employee_companyid_fkey FOREIGN KEY (companyid) REFERENCES company(id) ON UPDATE CASCADE ON DELETE CASCADE);
```

#### run application
```
mvn clean install jetty:run -Djetty.http.port=8080
```
