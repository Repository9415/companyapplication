package ru.ivan.myapp.backend.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Setter
@Getter
@ToString
@NoArgsConstructor
public class Company {
    private String id = UUID.randomUUID().toString();
    private String name = new String("");
    private String inn = new String("");
    private String address = new String("");
    private Long phoneNumber;

    public Company(@NotNull final String name,
                   @NotNull final String inn,
                   @NotNull final String address,
                   @NotNull final Long phoneNumber) {
        this.name = name;
        this.inn = inn;
        this.address = address;
        this.phoneNumber = phoneNumber;
    }
}
