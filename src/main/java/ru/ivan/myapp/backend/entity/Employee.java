package ru.ivan.myapp.backend.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.UUID;

@Setter
@Getter
@ToString
@NoArgsConstructor
public class Employee {
    private String id = UUID.randomUUID().toString();
    private String fio = new String();
    private Date dateBirth = new Date();
    private String email = new String();
    private String companyId = new String();

    public Employee(@NotNull final String fio,
                    @NotNull final Date dateBirth,
                    @NotNull final String email,
                    @NotNull final String companyId) {
        this.fio = fio;
        this.dateBirth = dateBirth;
        this.email = email;
        this.companyId = companyId;
    }
}
