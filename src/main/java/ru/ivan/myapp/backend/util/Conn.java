package ru.ivan.myapp.backend.util;

import org.apache.commons.dbcp.BasicDataSource;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class Conn {

    @NotNull
    private static final String fileName = "src/main/resources/database.properties";
    private static Conn connection = new Conn();
    private static Connection conn;
    private static BasicDataSource dataSource = new BasicDataSource();

    private Conn() {
        try {
            Class.forName(getProperty("driver_postgres"));
            conn = DriverManager.getConnection(getProperty("url_postgres"), getProperty("username_postgres"), getProperty("password"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @NotNull
    private static String getProperty(@NotNull final String propertyKey) {
        @NotNull final Properties props = new Properties();
        try (final InputStream in = Files.newInputStream(Paths.get(fileName))) {
            props.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        @NotNull final String value = props.getProperty(propertyKey);
        return value;
    }

    @NotNull
    public static Connection getConnection() {
        if (conn == null) {
            connection = new Conn();
        }
        return conn;
    }

    @NotNull
    public static Connection getConnectionBasicDS() throws Exception {
        dataSource.setDriverClassName(getProperty("driver_postgres"));
        dataSource.setUrl(getProperty("url_postgres"));
        dataSource.setUsername(getProperty("username_postgres"));
        dataSource.setPassword(getProperty("password"));

        return dataSource.getConnection();
    }

    @NotNull
    public static BasicDataSource getDataSource() {
        dataSource.setDriverClassName(getProperty("driver_postgres"));
        dataSource.setUrl(getProperty("url_postgres"));
        dataSource.setUsername(getProperty("username_postgres"));
        dataSource.setPassword(getProperty("password"));

        return dataSource;
    }
}
