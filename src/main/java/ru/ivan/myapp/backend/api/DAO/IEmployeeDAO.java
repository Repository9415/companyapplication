package ru.ivan.myapp.backend.api.DAO;

import ru.ivan.myapp.backend.entity.Employee;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

public interface IEmployeeDAO {

    void create(@NotNull final Employee employee);

    Employee getEmployee(@NotNull final String employeeId);

    List<Employee> listEmployees(@NotNull final String companyId);

    List<Employee> listEmployees();

    void delete(@NotNull final String employeeId);

    void update(@NotNull final Employee employee);
}
