package ru.ivan.myapp.backend.api.service;

import ru.ivan.myapp.backend.entity.Employee;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface IEmployeeService {
    @NotNull Employee findOne(@NotNull final String employeeId);

    @NotNull List<@NotNull Employee> findAll(@NotNull final String companyId);

    @NotNull List<@NotNull Employee> findAll();

    void persist(@NotNull final Employee employee);

    void merge(@NotNull final Employee employee);

    void remove(@NotNull final String employeeId);
}
