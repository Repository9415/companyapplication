package ru.ivan.myapp.backend.api.service;

import ru.ivan.myapp.backend.entity.Company;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface ICompanyService {
    @NotNull Company findOne(@NotNull final String companyId);

    @NotNull List<@NotNull Company> findAll();

    @NotNull List<@NotNull String> findAllNameCompanies();

    void persist(@NotNull final Company company);

    void merge(@NotNull final Company company);

    void remove(@NotNull final String companyId);
}
