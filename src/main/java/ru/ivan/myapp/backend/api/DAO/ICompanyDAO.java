package ru.ivan.myapp.backend.api.DAO;

import ru.ivan.myapp.backend.entity.Company;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface ICompanyDAO {

    void create(@NotNull final Company company);

    Company getCompany(@NotNull final String companyId);

    List<Company> listCompanies();

    void delete(@NotNull final String companyId);

    void update(@NotNull final Company company);
}
