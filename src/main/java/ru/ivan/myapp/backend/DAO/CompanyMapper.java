package ru.ivan.myapp.backend.DAO;

import org.springframework.jdbc.core.RowMapper;
import ru.ivan.myapp.backend.entity.Company;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CompanyMapper implements RowMapper {
    public Company mapRow(ResultSet rs, int rowNum) throws SQLException {
        Company company = new Company();
        company.setId(rs.getString("id"));
        company.setName(rs.getString("name"));
        company.setInn(rs.getString("inn"));
        company.setAddress(rs.getString("address"));
        company.setPhoneNumber(rs.getLong("phoneNumber"));
        return company;
    }
}
