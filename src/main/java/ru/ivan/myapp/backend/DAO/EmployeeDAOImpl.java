package ru.ivan.myapp.backend.DAO;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import ru.ivan.myapp.backend.api.DAO.IEmployeeDAO;
import ru.ivan.myapp.backend.entity.Employee;

import javax.sql.DataSource;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EmployeeDAOImpl implements IEmployeeDAO {
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public void setNamedParameterJdbcTemplate(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public void create(@NotNull final Employee employee) {
        @NotNull final String SQL = "INSERT INTO Employee (id, fio, dateBirth, email, companyId) " +
                "VALUES (:id, :fio, :dateBirth, :email, :companyId)";
        Map namedParameters = new HashMap();
        namedParameters.put("id", employee.getId());
        namedParameters.put("fio", employee.getFio());
        namedParameters.put("dateBirth", employee.getDateBirth());
        namedParameters.put("email", employee.getEmail());
        namedParameters.put("companyId", employee.getCompanyId());
        namedParameterJdbcTemplate.update(SQL, namedParameters);
    }

    @Override
    public void update(@NotNull final Employee employee) {
        @NotNull final String SQL = "UPDATE Employee SET fio = :fio, dateBirth = :dateBirth, email = :email, companyId = :companyId WHERE id = :id";
        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("id", employee.getId())
                .addValue("fio", employee.getFio())
                .addValue("dateBirth", employee.getDateBirth())
                .addValue("email", employee.getEmail())
                .addValue("companyId", employee.getCompanyId());
        namedParameterJdbcTemplate.update(SQL, namedParameters);
    }

    @Override
    public Employee getEmployee(@NotNull final String employeeId) {
        @NotNull final String SQL = "SELECT * FROM Employee WHERE id = :id";
        SqlParameterSource namedParameters = new MapSqlParameterSource("id", employeeId);
        Employee employee = (Employee) namedParameterJdbcTemplate.queryForObject(SQL, namedParameters, new EmployeeMapper());
        return employee;
    }

    @Override
    public List<Employee> listEmployees(@NotNull final String companyId) {
        @NotNull final String SQL = "SELECT * FROM Employee WHERE companyId = :companyId";
        SqlParameterSource namedParameters = new MapSqlParameterSource("companyId", companyId);
        List employees = (List) namedParameterJdbcTemplate.query(SQL, namedParameters, new EmployeeMapper());
        return employees;
    }

    @Override
    public List<Employee> listEmployees() {
        @NotNull final String SQL = "SELECT * FROM Employee";
        List employees = (List) namedParameterJdbcTemplate.query(SQL, new EmployeeMapper());
        return employees;
    }

    @Override
    public void delete(@NotNull final String employeeId) {
        @NotNull final String SQL = "DELETE FROM Employee WHERE id = :id";
        SqlParameterSource namedParameters = new MapSqlParameterSource("id", employeeId);
        namedParameterJdbcTemplate.update(SQL, namedParameters);
    }
}
