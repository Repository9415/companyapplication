package ru.ivan.myapp.backend.DAO;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import ru.ivan.myapp.backend.api.DAO.ICompanyDAO;
import ru.ivan.myapp.backend.entity.Company;

import javax.sql.DataSource;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CompanyDAOImpl implements ICompanyDAO {
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public void setNamedParameterJdbcTemplate(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public void create(@NotNull final Company company) {
        @NotNull final String SQL = "INSERT INTO Company (id, name, inn, address, phoneNumber) " +
                "VALUES (:id, :name, :inn, :address, :phoneNumber)";
        Map namedParameters = new HashMap();
        namedParameters.put("id", company.getId());
        namedParameters.put("name", company.getName());
        namedParameters.put("inn", Long.valueOf(roundDouble(company.getInn())));
        namedParameters.put("address", company.getAddress());
        namedParameters.put("phoneNumber", Long.valueOf(company.getPhoneNumber()));
        namedParameterJdbcTemplate.update(SQL, namedParameters);
    }

    @Override
    public Company getCompany(@NotNull final String companyId) {
        @NotNull final String SQL = "SELECT * FROM Company WHERE id = :id";
        SqlParameterSource namedParameters = new MapSqlParameterSource("id", companyId);
        Company company = (Company) namedParameterJdbcTemplate.queryForObject(SQL, namedParameters, new CompanyMapper());
        return company;
    }

    @Override
    public List<Company> listCompanies() {
        @NotNull final String SQL = "SELECT * FROM Company";
        List companies = (List) namedParameterJdbcTemplate.query(SQL, new CompanyMapper());
        return companies;
    }

    @Override
    public void delete(@NotNull final String companyId) {
        @NotNull final String SQL = "DELETE FROM Company WHERE id = :id";
        SqlParameterSource namedParameters = new MapSqlParameterSource("id", companyId);
        namedParameterJdbcTemplate.update(SQL, namedParameters);
    }

    @Override
    public void update(@NotNull final Company company) {
        @NotNull final String SQL = "UPDATE Company SET name = :name, inn = :inn, address = :address, phoneNumber = :phoneNumber WHERE id = :id";
        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("id", company.getId())
                .addValue("name", company.getName())
                .addValue("inn", Long.valueOf(roundDouble(company.getInn())))
                .addValue("address", company.getAddress())
                .addValue("phoneNumber", Long.valueOf(company.getPhoneNumber()));
        namedParameterJdbcTemplate.update(SQL, namedParameters);
    }

    private long roundDouble(String value) {
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(0, RoundingMode.HALF_UP);// 0 - округление знаков после запятой
        return bd.longValue();
    }
}
