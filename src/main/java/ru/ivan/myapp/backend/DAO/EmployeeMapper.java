package ru.ivan.myapp.backend.DAO;

import org.springframework.jdbc.core.RowMapper;
import ru.ivan.myapp.backend.entity.Company;
import ru.ivan.myapp.backend.entity.Employee;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EmployeeMapper implements RowMapper {
    public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
        Employee employee = new Employee();
        employee.setId(rs.getString("id"));
        employee.setFio(rs.getString("fio"));
        employee.setDateBirth(rs.getDate("dateBirth"));
        employee.setEmail(rs.getString("email"));
        employee.setCompanyId(rs.getString("companyId"));
        return employee;
    }
}
