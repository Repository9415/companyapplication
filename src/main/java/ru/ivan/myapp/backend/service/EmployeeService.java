package ru.ivan.myapp.backend.service;

import ru.ivan.myapp.backend.api.DAO.IEmployeeDAO;
import ru.ivan.myapp.backend.api.service.IEmployeeService;
import ru.ivan.myapp.backend.entity.Employee;

import javax.validation.constraints.NotNull;
import java.util.List;

public class EmployeeService implements IEmployeeService {
    private IEmployeeDAO employeeDAO;

    public EmployeeService(@NotNull final IEmployeeDAO employeeDAO) {
        this.employeeDAO = employeeDAO;
    }

    @NotNull
    @Override
    public Employee findOne(@NotNull final String employeeId) {
        return employeeDAO.getEmployee(employeeId);
    }

    @NotNull
    @Override
    public List<@NotNull Employee> findAll(@NotNull final String companyId) {
        return employeeDAO.listEmployees(companyId);
    }

    @NotNull
    @Override
    public List<@NotNull Employee> findAll() {
        return employeeDAO.listEmployees();
    }

    @Override
    public void persist(@NotNull final Employee employee) {
        employeeDAO.create(employee);
    }

    @Override
    public void merge(@NotNull final Employee employee) {
        employeeDAO.update(employee);
    }

    @Override
    public void remove(@NotNull final String employeeId) {
        employeeDAO.delete(employeeId);
    }
}
