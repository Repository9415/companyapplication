package ru.ivan.myapp.backend.service;

import ru.ivan.myapp.backend.api.DAO.ICompanyDAO;
import ru.ivan.myapp.backend.api.service.ICompanyService;
import ru.ivan.myapp.backend.entity.Company;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

public class CompanyService implements ICompanyService {
    private ICompanyDAO companyDAO;

    public CompanyService(@NotNull final ICompanyDAO companyDAO) {
        this.companyDAO = companyDAO;
    }

    @NotNull
    @Override
    public final Company findOne(@NotNull final String companyId) {
        return companyDAO.getCompany(companyId);
    }

    @NotNull
    @Override
    public List<@NotNull Company> findAll() {
        return companyDAO.listCompanies();
    }

    @NotNull
    @Override
    public List<@NotNull String> findAllNameCompanies() {
        List<String> listNameCompany = companyDAO.listCompanies().stream().map(Company::getName).collect(Collectors.toList());
        return listNameCompany;
    }

    @Override
    public void persist(@NotNull final Company company) {
        companyDAO.create(company);
    }

    @Override
    public void merge(@NotNull final Company company) {
        companyDAO.update(company);
    }

    @Override
    public void remove(@NotNull final String companyId) {
        companyDAO.delete(companyId);
    }
}
