package ru.ivan.myapp.frontend.api.grid;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface IGridEntity<T extends Object> {
    void create(T entity);

    void update(T entity);

    List<T> listAll();

    T findOne(String entityId);

    void remove(String entityId);

    void setVisible(boolean b);

    boolean isVisible();

    void setItems(Collection<T> items);

    Set<T> getSelectedItems();

    void openAddButtonDialog();

    void openEditButtonDialog();

    void openDeleteButtonDialog();
}
