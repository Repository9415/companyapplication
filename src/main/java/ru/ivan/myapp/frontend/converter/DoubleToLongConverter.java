package ru.ivan.myapp.frontend.converter;

import com.vaadin.flow.data.binder.Result;
import com.vaadin.flow.data.binder.ValueContext;
import com.vaadin.flow.data.converter.Converter;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class DoubleToLongConverter implements Converter<Double, Long> {

    @Override
    public Result<Long> convertToModel(Double fieldValue, ValueContext context) {
        // Produces a converted value or an error
        try {
            // ok is a static helper method that
            // creates a Result
            return Result.ok(roundDouble(String.valueOf(fieldValue)));
        } catch (NumberFormatException e) {
            // error is a static helper method
            // that creates a Result
            return Result.error("Enter a number");
        }
    }

    @Override
    public Double convertToPresentation(Long string, ValueContext context) {
        // Converting to the field type should
        // always succeed, so there is no support for
        // returning an error Result.
        return Double.valueOf(string);
    }

    private long roundDouble(String value) {
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(0, RoundingMode.HALF_UP);// 0 - округление знаков после запятой
        return bd.longValue();
    }
}
