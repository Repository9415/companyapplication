package ru.ivan.myapp.frontend.dialog;

import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.NativeButton;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import ru.ivan.myapp.backend.entity.Company;
import ru.ivan.myapp.frontend.view.TabLayoutView;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class DeleteCompanyDialog extends Dialog {

    public DeleteCompanyDialog(String nameDialog, TabLayoutView tabLayoutView) {
        super(new Label(nameDialog));
        NativeButton sendDeleteCompanyButton = new NativeButton("Delete", event -> {
            Set<Company> companySet = tabLayoutView.getSelectedGrid().getSelectedItems();
            List<Company> companySelectedList = new ArrayList<>(companySet);
            companySelectedList.forEach(e -> {
                tabLayoutView.getSelectedGrid().remove(e.getId());
            });
            this.close();
            tabLayoutView.getSelectedGrid().setItems(tabLayoutView.getSelectedGrid().listAll());
        });
        sendDeleteCompanyButton.addClickShortcut(Key.ENTER);
        NativeButton cancelDeleteCompanyButton = new NativeButton("Cancel", event -> {
            this.close();
        });
        HorizontalLayout buttonsLayout = new HorizontalLayout(sendDeleteCompanyButton, cancelDeleteCompanyButton);
        this.add(buttonsLayout);
    }
}
