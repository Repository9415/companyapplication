package ru.ivan.myapp.frontend.dialog;

import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.NativeButton;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.converter.LocalDateToDateConverter;
import com.vaadin.flow.data.validator.EmailValidator;
import ru.ivan.myapp.backend.entity.Company;
import ru.ivan.myapp.backend.entity.Employee;
import ru.ivan.myapp.frontend.view.TabLayoutView;

import java.sql.Timestamp;
import java.time.LocalDate;

public class AddOrEditEmployeeDialog extends Dialog {
    private final Notification errorNotification = new Notification("Please fill in the fields correctly!",
            3000, Notification.Position.MIDDLE);
    private Employee editEmployee = new Employee();
    private Binder<Employee> binderEmployee = new Binder<>(Employee.class);
    private Select<Company> listCompaniesSelect = new Select<>();
    private TabLayoutView tabLayoutView;
    private boolean addButtonClickMode = false;

    public AddOrEditEmployeeDialog(String nameDialog, TabLayoutView tabLayoutView) {
        super(new Label(nameDialog));
        this.tabLayoutView = tabLayoutView;

        TextField fioEmployeeTextField = new TextField("Full Name:");
        fioEmployeeTextField.setAutofocus(true);
        fioEmployeeTextField.setRequired(true);
        fioEmployeeTextField.setClearButtonVisible(true);
        fioEmployeeTextField.setErrorMessage("Please enter a valid Full Name!");

        DatePicker dateBirthEmployeeDatePicker = new DatePicker("Date Birth:");
        dateBirthEmployeeDatePicker.setRequired(true);
        dateBirthEmployeeDatePicker.setClearButtonVisible(true);
        dateBirthEmployeeDatePicker.setMax(LocalDate.from(new Timestamp(System.currentTimeMillis()).toLocalDateTime()));
        dateBirthEmployeeDatePicker.setErrorMessage("Please select a Date Birth!");

        EmailField emailEmployeeField = new EmailField("Email:");
        emailEmployeeField.setClearButtonVisible(true);
        emailEmployeeField.setErrorMessage("Please enter a valid Email address!");

        listCompaniesSelect.setLabel("Company:");
        listCompaniesSelect.setErrorMessage("Please select a Company!");
        listCompaniesSelect.setItemLabelGenerator(Company::getName);
        listCompaniesSelect.setItems(tabLayoutView.getCompanyGrid().listAll());

        HorizontalLayout textFieldsLayout = new HorizontalLayout(fioEmployeeTextField, dateBirthEmployeeDatePicker,
                emailEmployeeField, listCompaniesSelect);

        binderEmployee.forField(fioEmployeeTextField)
                .asRequired("Fio can't be empty")
                .withValidator(fio -> fio.length() > 0 && fio.length() < 20, "Fio is too long!")
                .bind(Employee::getFio, Employee::setFio);
        binderEmployee.forField(dateBirthEmployeeDatePicker)
                .asRequired("Date Birth can't be empty")
                .withConverter(new LocalDateToDateConverter())
                .bind(Employee::getDateBirth, Employee::setDateBirth);
        binderEmployee.forField(emailEmployeeField)
                .withValidator(new EmailValidator("Please enter a valid Email address!"))
                .bind(Employee::getEmail, Employee::setEmail);

        NativeButton sendAddOrEditEmployeeButton = new NativeButton("Confirm", event -> {
            this.close();
            try {
                Company company = listCompaniesSelect.getValue();
                editEmployee.setCompanyId(company.getId());
                binderEmployee.writeBean(editEmployee);

                if (addButtonClickMode) {
                    tabLayoutView.getSelectedGrid().update(editEmployee);
                } else {
                    tabLayoutView.getSelectedGrid().create(editEmployee);
                }
            } catch (Exception e) {
                if (errorNotification.isOpened()) {
                    return;
                } else {
                    errorNotification.open();
                    return;
                }
            }
            tabLayoutView.getSelectedGrid().setItems(tabLayoutView.getSelectedGrid().listAll());
        });
        NativeButton cancelEditEmployeeButton = new NativeButton("Cancel", event -> {
            this.close();
        });
        sendAddOrEditEmployeeButton.addClickShortcut(Key.ENTER);
        HorizontalLayout buttonsLayout = new HorizontalLayout(sendAddOrEditEmployeeButton, cancelEditEmployeeButton);
        this.add(textFieldsLayout, buttonsLayout);
    }

    public void setSelectedEmployee(Employee selectedEmployee) {
        editEmployee.setId(selectedEmployee.getId());
        editEmployee.setFio(selectedEmployee.getFio());
        editEmployee.setEmail(selectedEmployee.getEmail());
        editEmployee.setDateBirth(selectedEmployee.getDateBirth());
        editEmployee.setCompanyId(selectedEmployee.getCompanyId());
        listCompaniesSelect.setValue(tabLayoutView.getCompanyGrid().findOne(editEmployee.getCompanyId()));
        listCompaniesSelect.setPlaceholder((tabLayoutView.getCompanyGrid().findOne(editEmployee.getCompanyId())).getName());
        try {
            addButtonClickMode = true;
            binderEmployee.setBean(editEmployee);
            binderEmployee.readBean(editEmployee);
        } catch (Exception e) {
            if (errorNotification.isOpened()) {
                return;
            } else {
                errorNotification.open();
                this.close();
                return;
            }
        }
    }
}
