package ru.ivan.myapp.frontend.dialog;

import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.NativeButton;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import ru.ivan.myapp.backend.entity.Company;
import ru.ivan.myapp.frontend.converter.DoubleToLongConverter;
import ru.ivan.myapp.frontend.converter.DoubleToStringConverter;
import ru.ivan.myapp.frontend.view.TabLayoutView;

public class AddOrEditCompanyDialog extends Dialog {
    private final Notification errorNotification = new Notification("Please fill in the fields correctly!",
            3000, Notification.Position.MIDDLE);
    private Company editCompany = new Company();
    private Binder<Company> binderCompany = new Binder<>(Company.class);
    private boolean addButtonClickMode = false;

    public AddOrEditCompanyDialog(String nameDialog, TabLayoutView tabLayoutView) {
        super(new Label(nameDialog));

        TextField nameCompanyTextField = new TextField("Name:");
        nameCompanyTextField.setAutofocus(true);
        nameCompanyTextField.setRequired(true);
        nameCompanyTextField.setMaxLength(20);
        nameCompanyTextField.setClearButtonVisible(true);
        nameCompanyTextField.setErrorMessage("Please enter a valid Name!");

        NumberField innCompanyNumberField = new NumberField("Inn:");
        innCompanyNumberField.setClearButtonVisible(true);
        innCompanyNumberField.setErrorMessage("Please enter a valid Inn!");

        TextField addressCompanyTextField = new TextField("Address:");
        addressCompanyTextField.setMaxLength(20);
        addressCompanyTextField.setClearButtonVisible(true);
        addressCompanyTextField.setErrorMessage("Please enter a valid Address!");

        NumberField phoneNumberCompanyNumberField = new NumberField("Phone number:");
        phoneNumberCompanyNumberField.setClearButtonVisible(true);
        phoneNumberCompanyNumberField.setErrorMessage("Please enter a valid Phone number!");

        HorizontalLayout textFieldsLayout = new HorizontalLayout(nameCompanyTextField, innCompanyNumberField,
                addressCompanyTextField, phoneNumberCompanyNumberField);

        binderCompany.forField(nameCompanyTextField)
                .asRequired("Name company can't be empty")
                .withValidator(name -> name.length() > 0 && name.length() < 20, "Name is too long!")
                .bind(Company::getName, Company::setName);
        binderCompany.forField(innCompanyNumberField)
                .asRequired("Inn can't be empty")
                .withConverter(new DoubleToStringConverter())
                .withValidator(inn -> inn.length() >= 15 && inn.length() < 17, "Inn is too long or short!")
                .bind(Company::getInn, Company::setInn);
        binderCompany.forField(addressCompanyTextField)
                .asRequired("Address can't be empty")
                .withValidator(name -> name.length() > 0 && name.length() < 20, "Address is too long!")
                .bind(Company::getAddress, Company::setAddress);
        binderCompany.forField(phoneNumberCompanyNumberField)
                .asRequired("Phone Number can't be empty")
                .withConverter(new DoubleToLongConverter())
                .withValidator(phone -> phone.toString().length() > 9 && phone.toString().length() <= 13,
                        "Phone Number is too long or short!")
                .bind(Company::getPhoneNumber, Company::setPhoneNumber);

        NativeButton sendAddOrEditCompanyButton = new NativeButton("Confirm", event -> {
            this.close();
            try {
                binderCompany.writeBean(editCompany);
                if (addButtonClickMode) {
                    tabLayoutView.getSelectedGrid().update(editCompany);
                } else {
                    tabLayoutView.getSelectedGrid().create(editCompany);
                }
            } catch (Exception e) {
                if (errorNotification.isOpened()) {
                    return;
                } else {
                    errorNotification.open();
                    return;
                }
            }
            tabLayoutView.getSelectedGrid().setItems(tabLayoutView.getSelectedGrid().listAll());
        });
        NativeButton cancelEditCompanyButton = new NativeButton("Cancel", event -> {
            this.close();
        });
        sendAddOrEditCompanyButton.addClickShortcut(Key.ENTER);
        HorizontalLayout buttonsLayout = new HorizontalLayout(sendAddOrEditCompanyButton, cancelEditCompanyButton);
        this.add(textFieldsLayout, buttonsLayout);
    }

    public void setSelectedCompany(Company selectedCompany) {
        editCompany.setId(selectedCompany.getId());
        editCompany.setName(selectedCompany.getName());
        editCompany.setInn(selectedCompany.getInn());
        editCompany.setAddress(selectedCompany.getAddress());
        editCompany.setPhoneNumber(selectedCompany.getPhoneNumber());
        try {
            addButtonClickMode = true;
            binderCompany.setBean(editCompany);
            binderCompany.readBean(editCompany);
        } catch (Exception e) {
            if (errorNotification.isOpened()) {
                return;
            } else {
                errorNotification.open();
                this.close();
                return;
            }
        }
    }
}