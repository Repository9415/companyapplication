package ru.ivan.myapp.frontend.dialog;

import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.NativeButton;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import ru.ivan.myapp.backend.entity.Employee;
import ru.ivan.myapp.frontend.view.TabLayoutView;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class DeleteEmployeeDialog extends Dialog {

    public DeleteEmployeeDialog(String nameDialog, TabLayoutView tabLayoutView) {
        super(new Label(nameDialog));
        NativeButton sendDeleteEmployeeButton = new NativeButton("Delete", event -> {
            Set<Employee> employeeSet = tabLayoutView.getSelectedGrid().getSelectedItems();
            List<Employee> employeeSelectedList = new ArrayList<>(employeeSet);
            employeeSelectedList.forEach(e -> {
                tabLayoutView.getSelectedGrid().remove(e.getId());
            });
            this.close();
            tabLayoutView.getSelectedGrid().setItems(tabLayoutView.getSelectedGrid().listAll());
        });
        NativeButton cancelDeleteEmployeeButton = new NativeButton("Cancel", event -> {
            this.close();
        });
        sendDeleteEmployeeButton.addClickShortcut(Key.ENTER);
        HorizontalLayout buttonsLayout = new HorizontalLayout(sendDeleteEmployeeButton, cancelDeleteEmployeeButton);
        this.add(buttonsLayout);
    }
}
