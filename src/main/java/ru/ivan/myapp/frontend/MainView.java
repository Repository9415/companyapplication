package ru.ivan.myapp.frontend;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.PWA;
import ru.ivan.myapp.frontend.view.TabLayoutView;

@Route("")
@PWA(name = "Company Manager", shortName = "Company Manager")
public class MainView extends VerticalLayout {

    public MainView() {
        TabLayoutView tabLayoutView = new TabLayoutView();
        add(tabLayoutView);
    }
}
