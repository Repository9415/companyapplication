package ru.ivan.myapp.frontend.grid;

import com.vaadin.flow.component.grid.Grid;
import ru.ivan.myapp.backend.DAO.CompanyDAOImpl;
import ru.ivan.myapp.backend.api.service.ICompanyService;
import ru.ivan.myapp.backend.entity.Company;
import ru.ivan.myapp.backend.service.CompanyService;
import ru.ivan.myapp.backend.util.Conn;
import ru.ivan.myapp.frontend.api.grid.IGridEntity;
import ru.ivan.myapp.frontend.dialog.AddOrEditCompanyDialog;
import ru.ivan.myapp.frontend.dialog.DeleteCompanyDialog;
import ru.ivan.myapp.frontend.view.TabLayoutView;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public class CompanyGrid extends Grid<Company> implements IGridEntity<Company> {
    private final ICompanyService companyService;
    private final TabLayoutView tabLayoutView;

    public CompanyGrid(TabLayoutView tabLayoutView) {
        super(Company.class);
        CompanyDAOImpl companyDAO = new CompanyDAOImpl();
        companyDAO.setNamedParameterJdbcTemplate(Conn.getDataSource());
        this.companyService = new CompanyService(companyDAO);

        this.tabLayoutView = tabLayoutView;
        this.setItems(companyService.findAll());
        this.removeColumnByKey("id");
        this.setColumns("name", "inn", "address", "phoneNumber");
        this.setSelectionMode(Grid.SelectionMode.MULTI);

        this.addSelectionListener(selectionEvent -> {
            if (selectionEvent.getAllSelectedItems().isEmpty()) {
                tabLayoutView.setEnabledAddButton(true);
                tabLayoutView.setEnabledEditButton(false);
                tabLayoutView.setEnabledRemoveButton(false);
            } else if (selectionEvent.getAllSelectedItems().size() == 1) {
                tabLayoutView.setEnabledAddButton(true);
                tabLayoutView.setEnabledEditButton(true);
                tabLayoutView.setEnabledRemoveButton(true);
            } else if (selectionEvent.getAllSelectedItems().size() > 1) {
                tabLayoutView.setEnabledAddButton(true);
                tabLayoutView.setEnabledEditButton(false);
                tabLayoutView.setEnabledRemoveButton(true);
            }
        });
    }

    @Override
    public void create(Company entity) {
        companyService.persist(entity);
    }

    @Override
    public void update(Company entity) {
        companyService.merge(entity);
    }

    @Override
    public List<Company> listAll() {
        return companyService.findAll();
    }

    @Override
    public Company findOne(String entityId) {
        return companyService.findOne(entityId);
    }

    @Override
    public void remove(String entityId) {
        companyService.remove(entityId);
    }

    @Override
    public void setItems(Collection<Company> items) {
        super.setItems(companyService.findAll());
    }

    @Override
    public Set<Company> getSelectedItems() {
        return super.getSelectedItems();
    }

    @Override
    public void openAddButtonDialog() {
        AddOrEditCompanyDialog addCompanyDialog = new AddOrEditCompanyDialog("Enter company details", tabLayoutView);
        addCompanyDialog.open();
    }

    @Override
    public void openEditButtonDialog() {
        Set<Company> setEditCompany = tabLayoutView.getSelectedGrid().getSelectedItems();
        Company selectedCompany = setEditCompany.iterator().next();
        AddOrEditCompanyDialog editCompanyDialog = new AddOrEditCompanyDialog("Editing company", tabLayoutView);
        editCompanyDialog.setSelectedCompany(selectedCompany);
        editCompanyDialog.open();
    }

    @Override
    public void openDeleteButtonDialog() {
        new DeleteCompanyDialog("Remove selected companies?", tabLayoutView).open();
    }
}
