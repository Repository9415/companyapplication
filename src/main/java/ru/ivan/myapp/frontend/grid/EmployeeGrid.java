package ru.ivan.myapp.frontend.grid;

import com.vaadin.flow.component.grid.Grid;
import ru.ivan.myapp.backend.DAO.EmployeeDAOImpl;
import ru.ivan.myapp.backend.api.service.IEmployeeService;
import ru.ivan.myapp.backend.entity.Employee;
import ru.ivan.myapp.backend.service.EmployeeService;
import ru.ivan.myapp.backend.util.Conn;
import ru.ivan.myapp.frontend.api.grid.IGridEntity;
import ru.ivan.myapp.frontend.dialog.AddOrEditEmployeeDialog;
import ru.ivan.myapp.frontend.dialog.DeleteEmployeeDialog;
import ru.ivan.myapp.frontend.view.TabLayoutView;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public class EmployeeGrid extends Grid<Employee> implements IGridEntity<Employee> {
    private final IEmployeeService employeeService;
    private final TabLayoutView tabLayoutView;

    public EmployeeGrid(TabLayoutView tabLayoutView) {
        super(Employee.class);
        EmployeeDAOImpl employeeDAO = new EmployeeDAOImpl();
        employeeDAO.setNamedParameterJdbcTemplate(Conn.getDataSource());
        this.employeeService = new EmployeeService(employeeDAO);

        this.tabLayoutView = tabLayoutView;
        this.setItems(employeeService.findAll());
        this.removeColumnByKey("id");
        this.removeColumnByKey("companyId");
        this.setColumns("fio", "dateBirth", "email");
        this.addColumn(employee -> tabLayoutView.getCompanyGrid().findOne(employee.getCompanyId()).getName()).setHeader("Company");
        this.setSelectionMode(Grid.SelectionMode.MULTI);
        this.setVisible(false);

        this.addSelectionListener(selectionEvent -> {
            if (selectionEvent.getAllSelectedItems().isEmpty()) {
                tabLayoutView.setEnabledAddButton(true);
                tabLayoutView.setEnabledEditButton(false);
                tabLayoutView.setEnabledRemoveButton(false);
            } else if (selectionEvent.getAllSelectedItems().size() == 1) {
                tabLayoutView.setEnabledAddButton(true);
                tabLayoutView.setEnabledEditButton(true);
                tabLayoutView.setEnabledRemoveButton(true);
            } else if (selectionEvent.getAllSelectedItems().size() > 1) {
                tabLayoutView.setEnabledAddButton(true);
                tabLayoutView.setEnabledEditButton(false);
                tabLayoutView.setEnabledRemoveButton(true);
            }
        });
    }

    @Override
    public void create(Employee entity) {
        employeeService.persist(entity);
    }

    @Override
    public void update(Employee entity) {
        employeeService.merge(entity);
    }

    @Override
    public List<Employee> listAll() {
        return employeeService.findAll();
    }

    @Override
    public Employee findOne(String entityId) {
        return employeeService.findOne(entityId);
    }

    @Override
    public void remove(String entityId) {
        employeeService.remove(entityId);
    }

    @Override
    public void setItems(Collection<Employee> items) {
        super.setItems(employeeService.findAll());
    }

    @Override
    public Set<Employee> getSelectedItems() {
        return super.getSelectedItems();
    }

    @Override
    public void openAddButtonDialog() {
        AddOrEditEmployeeDialog addEmployeeDialog = new AddOrEditEmployeeDialog("Enter employee details", tabLayoutView);
        addEmployeeDialog.open();
    }

    @Override
    public void openEditButtonDialog() {
        Set<Employee> setEditEmployee = tabLayoutView.getSelectedGrid().getSelectedItems();
        Employee selectedEmployee = setEditEmployee.iterator().next();
        AddOrEditEmployeeDialog editEmployeeDialog = new AddOrEditEmployeeDialog("Editing employee", tabLayoutView);
        editEmployeeDialog.setSelectedEmployee(selectedEmployee);
        editEmployeeDialog.open();
    }

    @Override
    public void openDeleteButtonDialog() {
        new DeleteEmployeeDialog("Remove selected employees?", tabLayoutView).open();
    }
}
