package ru.ivan.myapp.frontend.view;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import lombok.Getter;
import ru.ivan.myapp.frontend.api.grid.IGridEntity;
import ru.ivan.myapp.frontend.grid.CompanyGrid;
import ru.ivan.myapp.frontend.grid.EmployeeGrid;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TabLayoutView extends VerticalLayout {

    @Getter
    private CompanyGrid companyGrid;
    private EmployeeGrid employeeGrid;
    private Tabs tabs;
    private Map<Tab, IGridEntity> tabsToGrids;
    private Button addButton;
    private Button editButton;
    private Button deleteButton;

    public TabLayoutView() {
        Tab companyTab = new Tab("Companies");
        Tab employeeTab = new Tab("Employees");
        tabs = new Tabs(companyTab, employeeTab);

        this.companyGrid = new CompanyGrid(this);
        this.employeeGrid = new EmployeeGrid(this);
        VerticalLayout grids = new VerticalLayout(this.companyGrid, this.employeeGrid);

        this.tabsToGrids = new HashMap<>();
        tabsToGrids.put(companyTab, this.companyGrid);
        tabsToGrids.put(employeeTab, this.employeeGrid);

        Set<IGridEntity> gridsShown = Stream.of(this.companyGrid, this.employeeGrid).collect(Collectors.toSet());
        tabs.addSelectedChangeListener(event -> {
            gridsShown.forEach(grid -> grid.setVisible(false));
            gridsShown.clear();
            IGridEntity selectedGrid = tabsToGrids.get(tabs.getSelectedTab());
            selectedGrid.setVisible(true);
            gridsShown.add(selectedGrid);
            this.companyGrid.setItems(companyGrid.listAll());
            this.employeeGrid.setItems(employeeGrid.listAll());
            if (companyGrid.listAll().isEmpty() && employeeGrid.isVisible()) {
                setEnabledAddButton(false);
                return;
            }
            setEnabledAddButton(true);
        });

        addButton = new Button("+Add", event -> {
            getSelectedGrid().openAddButtonDialog();
        });
        editButton = new Button("Edit", event -> {
            getSelectedGrid().openEditButtonDialog();
        });
        deleteButton = new Button("-Delete", event -> {
            getSelectedGrid().openDeleteButtonDialog();
        });
        editButton.setEnabled(false);
        deleteButton.setEnabled(false);
        HorizontalLayout buttons = new HorizontalLayout(addButton, editButton, deleteButton);
        add(buttons, tabs, grids);
    }

    public IGridEntity getSelectedGrid() {
        return this.tabsToGrids.get(tabs.getSelectedTab());
    }

    public void setEnabledAddButton(boolean b) {
        this.addButton.setEnabled(b);
    }

    public void setEnabledEditButton(boolean b) {
        this.editButton.setEnabled(b);
    }

    public void setEnabledRemoveButton(boolean b) {
        this.deleteButton.setEnabled(b);
    }
}
